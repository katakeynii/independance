  $(document).ready(function() {

    $('input#input_text, textarea#textarea1').characterCounter();
  

	$(function () {
	    $('#my_form').on('submit', function (e) {
	        // On empêche le navigateur de soumettre le formulaire
	        e.preventDefault();
	 
	        var $form = $(this);
	        var formdata = (window.FormData) ? new FormData($form[0]) : null;
	        var data = (formdata !== null) ? formdata : $form.serialize();	 
	        $.ajax({
	            url: $form.attr('action'),
	            type: $form.attr('method'),
	            contentType: false, // obligatoire pour de l'upload
	            processData: false, // obligatoire pour de l'upload
	            dataType: 'json', // selon le retour attendu
	            data: data,
	            success: function (response) {
	            	var metas = document.getElementsByTagName('meta'); 
	            	console.log(response);
	            	console.log(metas);
				    for (var i=0; i<metas.length; i++) { 
				       if (metas[i].getAttribute("property") == "og:image") { 
				        console.log(metas[i].getAttribute("content")); 
				        metas[i].setAttribute("content","http://localhost/independance/"+response.file);
				       
				      } 
				   } 
	            }
	        });
	    });
	    function submit_image(){
	        var $form = $("#my_form");
	        var formdata = (window.FormData) ? new FormData($form[0]) : null;
	        var data = (formdata !== null) ? formdata : $form.serialize();	 
	        $.ajax({
	            url: $form.attr('action'),
	            type: $form.attr('method'),
	            contentType: false, // obligatoire pour de l'upload
	            processData: false, // obligatoire pour de l'upload
	            dataType: 'json', // selon le retour attendu
	            data: data,
	            success: function (response) {
	                // La réponse du serveur
	                $result = $("#image_result");
	                $dwload = $("#download_pic");
	            	var metas = document.getElementsByTagName('meta'); 
	            	window.myImage = "http://gaindesoft.net/senegalais-fier/"+response.file; 
	            	$dwload.attr("href",window.myImage);
	            	// $dwload.attr("download", window.myImage);
	            	var image_meta = _.filter(metas, function(item){ 
	            		return item.getAttribute("property") == "og:image";
	            	});
	            	image_meta[0].setAttribute("content", "http://gaindesoft.net/senegalais-fier/"+response.file);
	            	console.log(image_meta[0].getAttribute("content"));
	                $result.attr('src', "");
	                $result.attr('src', response.file);
	            }
	        });
	    }
	    $('#sharePic').click(function(e){
				window.fbShare(window.myImage);
	    });
	    $('#my_form').find('input[name="image"]').on('change', function (e) {
	        var files = $(this)[0].files;
	 		console.log(files);
	        if (files.length > 0) {
	            // On part du principe qu'il n'y qu'un seul fichier
	            // étant donné que l'on a pas renseigné l'attribut "multiple"
	            var file = files[0],
	                $image_preview = $('#image_preview');
	 				$image_preview.attr('src', window.URL.createObjectURL(file));
	 				submit_image();
	            // Ici on injecte les informations recoltées sur le fichier pour l'utilisateur
/*	            $image_preview.find('.thumbnail').removeClass('hidden');
	            $image_preview.find('img').attr('src', window.URL.createObjectURL(file));
	            $image_preview.find('h4').html(file.name);
	            $image_preview.find('.caption p:first').html(file.size +' bytes');*/
	        }
	    });
	});
  });