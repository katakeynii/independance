<?php 
if (!session_id()) {
    session_start();
}
require_once __DIR__ . '../../vendor/autoload.php';

$fb = new Facebook\Facebook([
  'app_id' => '492079450998843', // Replace {app-id} with your app id
  'app_secret' => 'd8fc3731b111100c2509937c9e409cdb',
  'default_graph_version' => 'v2.2',
  ]);

$helper = $fb->getRedirectLoginHelper();

$permissions = ['email']; // Optional permissions
$loginUrl = $helper->getLoginUrl('http://localhost/independance/connecting/callback.php', $permissions);

echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
?>