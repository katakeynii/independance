
<?php
require_once __DIR__ . '/vendor/autoload.php';

$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["image"]["name"]);
$image = $_FILES;
if (move_uploaded_file($_FILES['image']['tmp_name'], $target_file)) {

	$source =$target_file;
} 
$flag_src ="rsz_1flag.png";
$star_src ="starjp.jpg";
$im = imagecreatefromjpeg($source);
$norm = imagecreatefromjpeg($source);
$flag = imagecreatefrompng($flag_src);
$star = imagecreatefromjpeg($star_src);

if($im && imagefilter($im, IMG_FILTER_GAUSSIAN_BLUR))
{
    $img = blur($im, 3);
	list($width, $height) = getimagesize($source);
	$bandeWidth = $width / 3;
	$largeWidth = ($width+200) / 3;
	$text_container = imagecreatetruecolor($width+100, 100);

	$vert = imagecolorallocatealpha($text_container, 0, 255, 0, 0);
	$jaune = imagecolorallocatealpha($text_container, 255, 255, 0, 0);
	$rouge = imagecolorallocatealpha($text_container, 255, 0, 0, 0);

	$vertAlpha = imagecolorallocatealpha($text_container, 0, 255, 0, 110);
	$jauneAlpha = imagecolorallocatealpha($text_container, 255, 255, 0, 110);
	$rougeAlpha = imagecolorallocatealpha($text_container, 255, 0, 0, 110);

	$black = imagecolorallocatealpha($text_container, 0, 0, 0, 40);
	$white = imagecolorallocatealpha($text_container, 255, 255, 255, 0);
	$flagX = ImageSX($flag);
	$flagY = ImageSY($flag);

	$blured = ImageResize($img, $width+200, $height+200);
	imagefilledrectangle($blured, 0, 0 , $largeWidth,  $height+200, $vertAlpha);
	imagefilledrectangle($blured, $largeWidth, 0, $largeWidth+$largeWidth,  $height+200, $jauneAlpha);
	imagefilledrectangle($blured, $largeWidth+$largeWidth, 0 , $largeWidth+$largeWidth+$largeWidth	,  $height+200, $rougeAlpha);

	imagecopymerge ( $blured, $norm , 100, 100, 0, 0, $width, $height, 100);
	$font_file = './big_noodle_titling.ttf';
	$ztH1 =(2*($height+200) / 3);
	$ztH2 =$ztH1+60;

	imagefilledrectangle($blured, 100, 100 , 100+$bandeWidth,  105, $vert);
	imagefilledrectangle($blured, 100+$bandeWidth, 100 , 100+$bandeWidth+$bandeWidth,  105, $jaune);
	imagefilledrectangle($blured, 100+$bandeWidth+$bandeWidth, 100 , 100+$bandeWidth+$bandeWidth+$bandeWidth	,  105, $rouge);


	imagefilledrectangle($blured, 0, $ztH1  , $width+200,  $ztH2, $black);
	imagefttext($blured, 30, 0, ($width/2)-30, $ztH2-10, $white, $font_file, 'SENEGALAIS ET FIER'); 
	imagecopymerge ( $blured, $flag, 0, $ztH1, 0, 0, $flagX, $flagY, 100);
	imagecopymerge ( $blured, $flag, $width+110 , $ztH1, 0, 0, $flagX, $flagY, 100);

/*	$starX=ImageSX($star);
	$starY=ImageSY($star);
	imagecopymerge ( $blured, $star, 100+$bandeWidth+($bandeWidth /2)-10 , 88, 0, 0, $starX, $starY, 100);*/
	$spikes = 5;

	$values = drawStar(25, 25, 20, $spikes);
	$st = imagecreatetruecolor(50,50);
	// imagecolorallocate($st,0,0,0);
	$noir = imagecolorallocate($im, 0, 0, 0);
	imagecolortransparent($st, $noir);
	$w = imagecolorallocate($st, 0, 255, 0);
	imagefilledpolygon($st, $values, $spikes*2, $w);
	imagecopymerge ( $blured, $st, 100+$bandeWidth+($bandeWidth /2)-25 , 75, 0, 0, 50, 50, 100);
    imagejpeg($blured, "final/".$target_file);
    $rep  = array("file"=>"final/".$target_file);
    echo json_encode($rep);
}
else
{
    echo 'Conversion to grayscale failed.';
}

imagedestroy($im);

function ImageResize($pImage, $t_width, $t_height) {
  // Target image
  $iCanvas = @ImageCreateTrueColor($t_width, $t_height);
  // Source dimensions
  $s_width = ImageSX($pImage);
  $s_height = ImageSY($pImage);
  // Copy image
  ImageCopyResampled($iCanvas, $pImage, 0, 0, 0, 0, $t_width, $t_height, $s_width, $s_height);
  // Return image
  return $iCanvas;
}

function blur($gdImageResource, $blurFactor = 3)
{
  // blurFactor has to be an integer
  $blurFactor = round($blurFactor);
 
  $originalWidth = imagesx($gdImageResource);
  $originalHeight = imagesy($gdImageResource);

  $smallestWidth = ceil($originalWidth * pow(0.5, $blurFactor));
  $smallestHeight = ceil($originalHeight * pow(0.5, $blurFactor));

  // for the first run, the previous image is the original input
  $prevImage = $gdImageResource;
  $prevWidth = $originalWidth;
  $prevHeight = $originalHeight;

  // scale way down and gradually scale back up, blurring all the way
  for($i = 0; $i < $blurFactor; $i += 1)
  {   
    // determine dimensions of next image
    $nextWidth = $smallestWidth * pow(2, $i);
    $nextHeight = $smallestHeight * pow(2, $i);

    // resize previous image to next size
    $nextImage = imagecreatetruecolor($nextWidth, $nextHeight);
    imagecopyresized($nextImage, $prevImage, 0, 0, 0, 0,
      $nextWidth, $nextHeight, $prevWidth, $prevHeight);

    // apply blur filter
    imagefilter($nextImage, IMG_FILTER_GAUSSIAN_BLUR);

    // now the new image becomes the previous image for the next step
    $prevImage = $nextImage;
    $prevWidth = $nextWidth;
      $prevHeight = $nextHeight;
  }

  // scale back to original size and blur one more time
  imagecopyresized($gdImageResource, $nextImage,
    0, 0, 0, 0, $originalWidth, $originalHeight, $nextWidth, $nextHeight);
  imagefilter($gdImageResource, IMG_FILTER_GAUSSIAN_BLUR);

  // clean up
  imagedestroy($prevImage);

  // return result
  return $gdImageResource;
}

function drawStar($x, $y, $radius, $spikes=5) {
    // $x, $y -> Position in the image
    // $radius -> Radius of the star
    // $spikes -> Number of spikes

    $coordinates = array();
    $angel = 360 / $spikes ;

    // Get the coordinates of the outer shape of the star
    $outer_shape = array();
    for($i=0; $i<$spikes; $i++){
        $outer_shape[$i]['x'] = $x + ($radius * cos(deg2rad(270 - $angel*$i)));
        $outer_shape[$i]['y'] = $y + ($radius * sin(deg2rad(270 - $angel*$i)));
    }

    // Get the coordinates of the inner shape of the star
    $inner_shape = array();
    for($i=0; $i<$spikes; $i++){
        $inner_shape[$i]['x'] = $x + (0.5*$radius * cos(deg2rad(270-180 - $angel*$i)));
        $inner_shape[$i]['y'] = $y + (0.5*$radius * sin(deg2rad(270-180 - $angel*$i)));
    }

    // Bring the coordinates in the right order
    foreach($inner_shape as $key => $value){
        if($key == (floor($spikes/2)+1))
             break;
        $inner_shape[] = $value;
        unset($inner_shape[$key]);
    }

    // Reset the keys
    $i=0;
    foreach($inner_shape as $value){
        $inner_shape[$i] = $value;
        $i++;
    }

    // "Merge" outer and inner shape
    foreach($outer_shape as $key => $value){
         $coordinates[] = $outer_shape[$key]['x'];
         $coordinates[] = $outer_shape[$key]['y'];
         $coordinates[] = $inner_shape[$key]['x'];
         $coordinates[] = $inner_shape[$key]['y'];
    }

    // Return the coordinates
    return $coordinates ;
}
?>
